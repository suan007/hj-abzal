### Hi there 👋, my name is Abzal Suan

<img src="./AbzalSuanBanner.png" />
<img src="https://www.codewars.com/users/AbzalSuan/badges/large" />

#### I am a Front-end developer
 I have  a results-focused attitude to creating SPA, using React(JS/TS), Redux.

 My free time is dedicated to Codewars, programming tutorials, and becoming familiar with NodeJS, because, in the future, I see myself as a Full Stack Developer.




Skills: React, Redux, JavaScript, TypeScript, HTML5, CSS3, Unit Tests, SnapShot, Storybook



[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/github.svg' alt='github' height='40'>](https://github.com/https://github.com/hj-abzal)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/linkedin.svg' alt='linkedin' height='40'>](https://www.linkedin.com/in/https://www.linkedin.com/in/abzal-suan//)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/instagram.svg' alt='instagram' height='40'>](https://www.instagram.com/https://www.instagram.com/hj_abzal//)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/icloud.svg' alt='website' height='40'>](https://hyojeong-abzal.github.io/my-portfolio/)  

